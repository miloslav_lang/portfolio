var express = require('express');
var router = express.Router();

//this url would be https.../users
//index url would be https.../

/* GET /users */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

//this url would be https.../users/create
/* GET /users/create */
router.get('/create', function(req, res, next) {
  var vm = {
    title: 'Create'
  };
  res.render('users/create', vm);
});

router.post('/create', function(req, res, next) {
  var somethingGoesWrong = false;  
  if (somethingGoesWrong) {  
    var vm = {
      title: 'Create an account',
      input: req.body, 
      error: 'Something went wrong'
    };
    delete vm.input.password;
    return res.render('users/create', vm);
  }
  res.redirect('/orders');  
});


module.exports = router;
  