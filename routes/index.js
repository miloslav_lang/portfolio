//routes also knows as MODEL
var express = require('express');
var router = express.Router();

var handlebars = require('hbs');

//good for accesing functions in other files http://stackoverflow.com/questions/5797852/in-node-js-how-do-i-include-functions-from-my-other-files
//var Spry = require('file location from this one');
//console.log(typeof does.this.function);


/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
//   //index goes to views index.hbs
// });


// var analytics = function analytics() {
//   var ga;
//   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

//   ga('create', 'UA-68930288-1', 'auto');
//   ga('send', 'pageview');
// };


/* GET /home or / */
router.get('/', function(req, res, next) {

  var date_result = null;

  function date_run() {
    var dayNames = new Array("Sunday", "Monday", "Tuesday", "Wednesday",
      "Thursday", "Friday", "Saturday");
    var monthNames = new Array(
      "January", "February", "March", "April", "May", "June", "July",
      "August", "September", "October", "November", "December");
    var now = new Date();

    // document.getElementById("date").innerHTML 
    date_result = (dayNames[now.getDay()] + ", " +
      monthNames[now.getMonth()] + " " +
      now.getDate() + ", " + now.getFullYear());
  }
  date_run();
//example of handlerbars helper  
// handlebars.registerHelper('agree_button', function() {
//   var emotion = Handlebars.escapeExpression(this.emotion),
//       name = Handlebars.escapeExpression(this.name);

//   return new Handlebars.SafeString(
//     "<button>I agree. I " + emotion + " " + name + "</button>"
//   );
// });

//mouseover on my face image
  // handlebars.registerHelper('fade_hover', function (targetElement, duration, from, to, toggle) {
  //   console.log('register helper yeayyy');
  //   Spry.Effect.DoFade(targetElement, {
  //     duration: duration,
  //     from: from,
  //     to: to,
  //     toggle: toggle
  //   });
  // });



  var vm = {
    title: 'Home',
    date: date_result
  };
  res.render('index', vm);
});





/* GET /about page. */
router.get('/about', function(req, res, next) {
  var vm = {
    title: 'About',
    //analytics: analytics
  };
  res.render('about', vm);
});

/* GET /development page. */
router.get('/development', function(req, res, next) {
  var vm = {
    title: 'Development',
    //analytics: analytics
  };
  res.render('development', vm);
});

/* GET /resume page. */
router.get('/resume', function(req, res, next) {
  var vm = {
    title: 'Resume',
    //analytics: analytics
  };
  res.render('resume', vm);
});



module.exports = router;
