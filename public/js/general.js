// var Handlebars = require('hbs');
//test1
// Handlebars.registerHelper();

(function() {

"use strict";

function resizeRefresh(){
	//fix a resize bug and nav
	window.onresize = function(){
		location.reload();
		console.log("RESIZED");
	};
}

//setTimeout(timer, 3000);
//function timer(){}

//if resume page is up and its in desktop view removes 1 nav item and moves others so they line up. mobile view is unaffected
	// if (window.location.pathname === ("/resume") ) {

	// 	if(window.innerWidth >= 983){
	// 		console.log("YES RESUME ");
	// 		var nav = document.getElementById("nav_li");
	// 		nav.style.marginLeft = "558px";
	// 	}
	// }

//if index page is up and its in desktop view removes 1 nav item and moves others so they line up. mobile view is unaffected
	if (window.location.pathname === ("/") ) {

		var nav_li2 = document.getElementById("nav_li");
		nav_li2.setAttribute("hidden", true);

		if(window.innerWidth >= 983){
			// console.log("YES INDEX ");
			var nav2 = document.getElementById("nav_li2");
			// nav2.style.marginLeft = "558px";
			nav2.style.marginLeft = "696px";
		}
	}

if(window.location.pathname === ("/") ) {

	function date_run(){
	    var dayNames = new Array("Sunday","Monday","Tuesday","Wednesday",
	    "Thursday","Friday","Saturday");

	    var monthNames = new Array(
	    "January","February","March","April","May","June","July",
	    "August","September","October","November","December");

	    var now = new Date();

	    document.getElementById("date").innerHTML = (dayNames[now.getDay()] + ", " +
	    monthNames[now.getMonth()] + " " +
	    now.getDate() + ", " + now.getFullYear());
	    }
	    date_run();
};



function analytics(){
	console.log("analytics ran");
	var ga;

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68930288-1', 'auto');
  ga('send', 'pageview');

}


// Handlebars.registerHelper("analytics", function() {
//   	console.log("hbs helper ran");
//});



})();
